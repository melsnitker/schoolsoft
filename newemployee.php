<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Employee</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Employees - Add New Hire</h3>";
			
			//query database for role labels
			$sql="SELECT * FROM roles";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<form action='confirmemployee.php' method='post' style='display:inline;'>
					<label for='first_name'>First Name</label>
					<input type='text' id='first_name' name='first_name' required='required'>
					<label for='last_name'>Last Name</label>
					<input type='text' id='last_name' name='last_name' required='required'>
					<label for='phone'>Phone</label>
					<input type='text' id='phone' name='phone' required='required'>
					<label for='role'>Position</label>
					<select id='role' name='role' required='required'>";
					while ($row = $result->fetch_assoc()){
						$thisroleid=$row['roleID'];
						$thisrole=$row['role'];
						echo "<option value=$thisroleid>$thisrole</option>";
					}
					echo "</select>
					<label for='gradelevel'>Grade Level (Teacher Only)</label>
					<input type='number' id='gradelevel' name='gradelevel'><br>
					<label for='roomnumber'>Room Number (Teacher Only)</label>
					<input type='number' id='roomnumber' name='roomnumber'><br>
					<label for='username'>Username</label>
					<input type='text' id='username' name='username' required='required'>
					<label for='password'>Password</label>
					<input type='text' id='password' name='password' required='required'>
					<button type='submit'>Save</button>
					</form>&emsp;&emsp;
					<form action='editemployees' style='display:inline;'><button type='submit'>Back to Employees</button></form>
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
