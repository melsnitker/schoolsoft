<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Add Assignment</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION["classperiod"])){
			    $class_period = $_SESSION["classperiod"];
		        $_SESSION["classperiod"] = $class_period;
			}
			if(isset($_SESSION["teacherID"])){
		        $teacherID = $_SESSION["teacherID"];
			    $_SESSION["teacherID"] = $teacherID;
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Add an Assignment</h3>";
			
			if(isset($_POST["assignment"])){
			    
			    //insert new assignment
			    $thisassignment = $_POST["assignment"];
			    $sql="INSERT INTO assignment (teacherID, class_period, assignment_name)
				    VALUES ($teacherID, $class_period, '$thisassignment')";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    
			    //insert into gradebook
			    $sql="SELECT DISTINCT studentID FROM takes WHERE teacherID=$teacherID AND class_period=$class_period";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    while($row = $result->fetch_assoc()){
					$thisstudent = $row["studentID"];
					$qry = "INSERT INTO gradebook (teacherID, class_period, assignment_name, studentID)
					    VALUES ($teacherID, $class_period, '$thisassignment', $thisstudent)";
					$result2=mysqli_query($con, $qry) or die($con->error); 
					    
				}
			    
			    echo "<h4>Assignment Successfully Added<h4>";
			}
			
			echo "&emsp;&emsp;<form action='teacherDashboard.php' style='display:inline;'><button type='submit'>Back to Home</button></form>";
			echo "&emsp;&emsp;<form action='gradebook.php' style='display:inline;'><button type='submit'>Back to Gradebook</button>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
