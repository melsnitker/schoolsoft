<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Student Allergy</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			if(isset($_SESSION["newstudent"])){
			    $thisstudent = $_SESSION["newstudent"];
			    $_SESSION["newstudent"] = $thisstudent;
		    } elseif(isset($_SESSION["editstudent"])){
				$thisstudent = $_SESSION["editstudent"];
				$_SESSION["editstudent"] = $thisstudent;
				
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Student Allergy</h3>";
			
			$thisstudent=$_POST["student"];
			$allergy=$_POST["allergy"];
			
			if(isset($_POST["new"])){
			    //insert allergy info
			    $sql="INSERT INTO allergy (studentID, allergy)
				    VALUES ($thisstudent, '$allergy')";
			    $result=mysqli_query($con, $sql) or die($con->error);
			}elseif(isset($_POST["delete"])){
			
			    //delete allergy
			    $sql="DELETE FROM allergy WHERE studentID=$thisstudent AND allergy='$allergy'";
			    $result=mysqli_query($con, $sql) or die($con->error);
			
			}else{
				
				
				//update allergy info
				$sql="UPDATE allergy SET allergy='$allergy'
				    WHERE studentID=" . $thisstudent;
			    $result=mysqli_query($con, $sql) or die($con->error);
			}
			
			echo "<h4>Change Successful</h4>";
			echo "&emsp;&emsp;<form action='allergies.php' style='display:inline;'><button type='submit'>Back to Allergies</button></form><br><br>";
			echo "&emsp;&emsp;<form action='nurseDashboard.php' style='display:inline;'><button type='submit'>Home</button></form>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
