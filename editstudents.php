<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Student Information</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		
		    $fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
		
		    if(isset($_SESSION["studentID"])){
			    unset($_SESSION["studentID"]);
			}
			if(isset($_POST["studentID"])){
			    unset($_POST["studentID"]);
			}
			if(isset($_SESSION["delete"])){
			    unset($_SESSION["delete"]);
			}
		    if(isset($_POST["delete"])){
			    unset($_POST["delete"]);
			}
			if(isset($_SESSION["new"])){
			    unset($_SESSION["new"]);
			}
			if(isset($_POST["new"])){
			    unset($_POST["new"]);
			}
			if(isset($_SESSION["existing"])){
				unset($_SESSION["existing"]);
			}
			if(isset($_POST["existing"])){
				unset($_POST["existing"]);
			}
			
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students</h3>";
			
			echo "<form action='newstudent.php'><button type='submit'>New Student</button></form><br><br>";
			
			//query database for students
			$sql="SELECT studentID, first_name, last_name FROM student ORDER BY last_name";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>
			    <form action='updatestudent.php' method='post'>";
				//Header of table
				echo "<table>
					<tr>
						<th>Student Name</th>
						<th></th>
						<th></th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$thisname = $row['first_name'] . ' ' . $row['last_name'];
						$studentID = $row['studentID'];
						echo "<tr><td>$thisname</td>
							<td><button type='submit' id='delete' name='delete' value='$studentID'>Unregister</button></td>
							<td><button type='submit' name='studentID' value='$studentID'>Edit</button></td></tr>"; 
					  }
				echo "</table></form>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
