<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Update Grades</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION["classperiod"])){
			    $class_period = $_SESSION["classperiod"];
		        $_SESSION["classperiod"] = $class_period;
			}
			if(isset($_SESSION["teacherID"])){
		        $teacherID = $_SESSION["teacherID"];
			    $_SESSION["teacherID"] = $teacherID;
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			
			if(isset($_POST["assignment"])){
				
				$thisassignment = $_POST["assignment"];
			    $_SESSION["assignment"] = $thisassignment;
			    echo "<h3>Update Grades - $thisassignment - Period $class_period</h3>";
			    
			    $students = array();
			    
			    //update grades
			    $sql="SELECT DISTINCT studentID FROM gradebook WHERE teacherID=$teacherID AND class_period=$class_period AND assignment_name='$thisassignment'";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    while($row=$result->fetch_assoc()){
					$sID=$row["studentID"];
					$grade = $_POST["student"][$sID];
					
					$students[$sID] = $grade;
				}
				
				foreach($students as $s => $gr){
			       
			        $qry = "UPDATE gradebook SET grade=$gr
			            WHERE teacherID=$teacherID AND class_period=$class_period AND assignment_name='$thisassignment' AND studentID=$s";
			        $result=mysqli_query($con, $qry) or die($con->error);
					    
				}
			    
			    echo "<h4>Grades Successfully Updated<h4>";
			}
			
			echo "&emsp;&emsp;<form action='coursegrades.php' style='display:inline;'><button type='submit'>Back to Roster</button></form>";
			echo "&emsp;&emsp;<form action='gradebook.php' style='display:inline;'><button type='submit'>Back to Gradebook</button></form><br><br>";
			echo "&emsp;&emsp;<form action='teacherDashboard.php' style='display:inline;'><button type='submit'>Back to Home</button></form>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
