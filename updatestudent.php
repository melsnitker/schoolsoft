<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Employee</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			if(isset($_SESSION["editstudent"]))
			{
				unset($_SESSION["editstudent"]);
			}
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Update Information</h3>";
			
			if(isset($_POST["delete"])){
				
				$thisstudent = $_POST["delete"];
				$_SESSION["editstudent"] = $thisstudent;
				//query database for this student's current information
				$qry="SELECT first_name, last_name FROM student WHERE studentID=$thisstudent";
			    $rslt=mysqli_query($con, $qry) or die($con->error);
			    $rw= $rslt->fetch_assoc();
			    $studentname = $rw['first_name'] . ' ' . $rw['last_name'];
			    $_SESSION["editname"]=$studentname;
			    echo "<h3>Are you sure you want to do this?</h3>
			          <h4>Unregister " . $studentname . "?</h4>
			          <form action='updatestudentcomplete.php' method='post' style='display:inline;'>
			          <button type='submit' name='delete' id='delete' value='$thisstudent'>Yes, Remove</button></form>&emsp;&emsp;
			          <form action='editstudents.php' style='display:inline;'><button type='submit'>Back</button></form>";
			
			}else{
			
			//query database for this student's current information
			
			if(isset($_POST["studentID"])){
			    $thisstudent = $_POST["studentID"];
			    $_SESSION["editstudent"] = $thisstudent;
			}
			$qry="SELECT first_name, last_name, middle_initial, grade_level, teacherID FROM student INNER JOIN takes ON student.studentID=takes.studentID AND student.studentID=" . $thisstudent;
			$rslt=mysqli_query($con, $qry) or die($con->error);
			$rw= $rslt->fetch_assoc();
			$currentteachid = $rw["teacherID"];
			
			//query for current homeroom teacher name
			$sql="SELECT first_name, last_name FROM teacher_info WHERE teacher_info.teacherID=" . $currentteachid;
			$rslt2=mysqli_query($con, $sql) or die($con->error);
			$rw2= $rslt2->fetch_assoc();
			$tname=$rw2["first_name"] .  " " . $rw2["last_name"];
			
			//query for teacher list
			$sql2="SELECT first_name, last_name, teacherID FROM teacher_info";
			$result2=mysqli_query($con, $sql2) or die($con->error);
			
			
			echo "<form action='updatestudentcomplete.php' method='post' style='display:inline;'>
					<label for='first_name'>First Name</label>
					<input type='text' id='first_name' name='first_name' value='" . $rw['first_name'] . "'>
					<label for='middleinitial'>Middle Initial</label>
					<input type='text' id='middleinitial' name='middleinitial' value='" . $rw['middle_initial'] . "'>
					<label for='last_name'>Last Name</label>
					<input type='text' id='last_name' name='last_name' value='" . $rw['last_name'] . "'>
					<label for='gradelevel'>Grade Level</label>
					<input type='number' id='gradelevel' name='gradelevel' value='" . $rw['grade_level'] . "'>
					<label for='homeroomteacher'>Homeroom Teacher</label>
					<select id='homeroomteacher' name='homeroomteacher'>
						<option value=" . $currentteachid . ">".$tname."</option>";
						while($row2=$result2->fetch_assoc()){
							$thisid=$row2["teacherID"];
							$thisname=$row2["first_name"] .  " " . $row2["last_name"];
							echo "<option value=" . $thisid . ">".$thisname."</option>";
						}
					echo "</select>
					<button type='submit'>Update</button></form>&emsp;&emsp;
					<form action='editstudents.php' style='display:inline;'><button type='submit'>Back</button></form>
					
					";
			}
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
