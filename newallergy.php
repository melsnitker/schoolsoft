<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Allergy</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - New Allergy</h3>";
			
			if(isset($_POST["existing"])){
				$_SESSION["existing"] = $_POST["existing"];
			}else{
				unset($_SESSION["existing"]);
			}
			if(isset($_SESSION["newstudent"])){
			    $thisstudent = $_SESSION["newstudent"];
			    $_SESSION["newstudent"] = $thisstudent;
		    } elseif(isset($_SESSION["editstudent"])){
				$thisstudent = $_SESSION["editstudent"];
				$_SESSION["editstudent"] = $thisstudent;
				
			}
			
			//query for list of current students
			$sql="SELECT first_name, last_name, studentID FROM student";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			
			echo "<form action='insertallergy.php' method='post'>
			        <input type='hidden' name='new' id='new' value ='new'>
					<label for='student'>Student</label>
					<select id='student' name='student' required='required'>";
					while($row = $result->fetch_assoc()){
						$sname = $row["first_name"] . " " . $row["last_name"];
						$sid = $row["studentID"];
						echo "<option value=$sid>$sname</option>";
					}
			echo "</select>
					<label for='allergy'>Allergy</label>
					<input type='text' id='allergy' name='allergy' required='required'>
					&emsp;&emsp;<button type='submit'>Save</button></form>&emsp;&emsp;
					<form action='allergies.php' style='display:inline;'><button type='submit'>Back to Allergies</button></form><br><br>
					&emsp;&emsp;<form action='nurseDashboard.php' style='display:inline;'><button type='submit'>Back to Home</button></form>
					
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
