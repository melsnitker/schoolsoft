<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Parent Info</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION["newstudent"])){
			    $thisstudent = $_SESSION["newstudent"];
			    $_SESSION["newstudent"] = $thisstudent;
		    } elseif(isset($_SESSION["editstudent"])){
				$thisstudent = $_SESSION["editstudent"];
				$_SESSION["editstudent"] = $thisstudent;
				
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - New Parent Info</h3>";
			
			
			if(isset($_POST["email"]))
			{
			    $email=$_POST["email"];
			}else{
				$email=NULL;
			}
			$parentfirstname=$_POST["p_first_name"];
			$parentlastname=$_POST["p_last_name"];
			$phone=$_POST["p_phone"];
			$phonetype=$_POST["phonetype"];
			$relationship=$_POST["relationship"];
			
			if(isset($_POST["new"])){
			
			    //insert new parent
		        $sql="INSERT INTO parent (first_name, last_name, email)
				    VALUES ('$parentfirstname', '$parentlastname', '$email')";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    $newparentid = mysqli_insert_id($con);
			
			    //insert parent phone info
			    $sql="INSERT INTO phone_contact_parent (parentID, phone_number, type)
			        VALUES ($newparentid, '$phone', '$phonetype')";
			    $result=mysqli_query($con, $sql) or die($con->error);
			
			    //insert into parent_to_student
		        $sql="INSERT INTO parent_to_student (parentID, studentID, relationship)
	                VALUES ($newparentid, $thisstudent, '$relationship')";
			    $result=mysqli_query($con, $sql) or die($con->error);
		    }elseif(isset($_POST["delete"])){
			    $thisparent = $_SESSION["editparent"];
				$_SESSION["editparent"] = $thisparent;
			
			    //delete parent
			    $sql="DELETE FROM parent WHERE parentID=$thisparent";
			    $result=mysqli_query($con, $sql) or die($con->error);
			
			}else{
				$thisparent = $_SESSION["editparent"];
				$_SESSION["editparent"] = $thisparent;
				
				//update parent
				$sql="UPDATE parent SET first_name='$parentfirstname', last_name='$parentlastname', email='$email' WHERE parentID=$thisparent";
				$result=mysqli_query($con, $sql) or die($con->error);
				
				//update parent phone info
				$sql="UPDATE phone_contact_parent SET phone_number='$phone', type='$phonetype' WHERE parentID=$thisparent";
				$result=mysqli_query($con, $sql) or die($con->error);
				
				//update parent_to_student - this is buggy!!!! AAARRRGGGGHHH
				//$sql="UPDATE parent_to_student SET relationship='$relationship' WHERE parentID=" . $thisparent . " AND studentID=" . $thisstudent;
				$sql="UPDATE parent_to_student SET relationship='$relationship' WHERE studentID=$thisstudent AND parentID=$thisparent";
				$result=mysqli_query($con, $sql) or die($con->error);
				
			}
			
			
			echo "<form action='newemergencyinfo.php' method='post' style='display:inline;'>
					&emsp;&emsp;<button type='submit'>Add Emergency Contact</button></form>&emsp;&emsp;
					<form action='newparentinfo.php' style='display:inline;'><button type='submit'>Add Another Parent</button></form><br><br>";
			echo "&emsp;&emsp;<form action='editstudents.php' style='display:inline;'><button type='submit'>Manage Students</button></form>";
			echo "&emsp;&emsp;<form action='studentcontact.php' style='display:inline;'><button type='submit'>Student Contacts</button></form>";
			
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
