<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Rosters</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			//unset relevant roster-viewing variables
			if(isset($_POST["teacherID"])){
				unset($_POST["teacherID"]); 
			}
			if(isset($_SESSION["teacherID"])){
				unset($_SESSION["teacherID"]); 
			}
			if(isset($_POST["teacherID"])){
				unset($_POST["teacherID"]); 
			}
			if(isset($_SESSION["teacherbyadmin"])){
				unset($_SESSION["teacherbyadmin"]); 
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h2>Choose a Teacher to View Rosters</h2>";
			
			//query database for teachers
			$sql="SELECT adminID, first_name, last_name, teacherID FROM teacher_info";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>";
				echo "<form action='roster.php' method='post'>";
				//Header of table
				echo "<table>
					<tr>
						<th>Teacher Name</th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$name = $row['first_name'] . ' ' . $row['last_name'];
						$admin = $row['adminID'];
						$teacherID=$row['teacherID'];
						echo "<tr>";
						echo "<td><button type='submit' name='teacherID' value='$teacherID' class='tablebutton'>$name</button></td></tr>"; 
					  }
				echo "</table>";
				echo "</form>";
			echo "</div>";
			$_SESSION["firstName"] = $fname;
			$_SESSION["lastName"] = $lname;
			$_SESSION["role"] = $role;
		?>
		
	</div>
	<br>

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
