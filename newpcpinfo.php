<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Physician</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - New Physician Contact</h3>";
			
			if(isset($_POST["existing"])){
				$_SESSION["existing"] = $_POST["existing"];
			}else{
				unset($_SESSION["existing"]);
			}
			if(isset($_POST["delete"])){
				unset($_POST["delete"]);
			}
			if(isset($_SESSION["newstudent"])){
			    $thisstudent = $_SESSION["newstudent"];
			    $_SESSION["newstudent"] = $thisstudent;
		    } elseif(isset($_SESSION["editstudent"])){
				$thisstudent = $_SESSION["editstudent"];
				$_SESSION["editstudent"] = $thisstudent;
				
			}
			
			echo "<form action='insertpcp.php' method='post' style='display:inline;'>
			        <input type='hidden' name='new' id='new' value ='new'>
					<label for='first_name'>First Name</label>
					<input type='text' id='first_name' name='first_name' required='required'>
					<label for='last_name'>Last Name</label>
					<input type='text' id='last_name' name='last_name' required='required'>
					<label for='phone'>Phone</label>
					<input type='text' id='phone' name='phone' required='required'>
					&emsp;&emsp;<button type='submit'>Next</button></form>&emsp;&emsp;
					<form action='editstudents.php' style='display:inline;'><button type='submit'>Back to Students</button></form>
					
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
