<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Parent Info</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - New Parent Info</h3>";
			
			if(isset($_POST["existing"])){
				$_SESSION["existing"] = $_POST["existing"];
			}else{
				unset($_SESSION["existing"]);
			}
			if(isset($_POST["delete"])){
				unset($_POST["delete"]);
			}
			
			if(isset($_POST["new"]))
			{
			    $newfirstname=$_POST["s_first_name"];
			    $newlastname=$_POST["last_name"];
			    $newmiddle=$_POST["middleinitial"];
			    $newgradelevel=$_POST["gradelevel"];
			    $newhomeroom=$_POST["homeroomteacher"];
			
		    	//insert new student
			    $sql="INSERT INTO student (first_name, last_name, middle_initial, grade_level)
				    VALUES ('$newfirstname', '$newlastname', '$newmiddle', $newgradelevel)";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    $newstudentid = mysqli_insert_id($con);
			
			    //insert into homeroom roster
			    $_SESSION["newstudent"] = $newstudentid;
			    $sql="INSERT INTO takes (studentID, class_period, teacherID, course_grade)
				    VALUES ($newstudentid, 1, $newhomeroom, 95)";
			    $result=mysqli_query($con, $sql) or die($con->error);
		    }
			
			echo "<form action='insertparent.php' method='post' style='display:inline;'>
			        <input type='hidden' name='new' id='new' value ='new'>
					<label for='p_first_name'>First Name</label>
					<input type='text' id='p_first_name' name='p_first_name' required='required'>
					<label for='p_last_name'>Last Name</label>
					<input type='text' id='p_last_name' name='p_last_name' required='required'>
					<label for='email'>Email</label>
					<input type='email' id='email' name='email'>
					<label for='p_phone'>Phone</label>
					<input type='text' id='p_phone' name='p_phone' required='required'>
					<label for='phonetype'>Phone Type</label>
					<input type='text' id='phonetype' name='phonetype' required='required'>
					<label for='relationship'>Relationship to Student</label>
					<input type='text' id='relationship' name='relationship' required='required'><br><br>
					&emsp;&emsp;<button type='submit'>Next</button></form>&emsp;&emsp;
					<form action='editstudents.php' style='display:inline;'><button type='submit'>Back to Students</button></form>
					
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
