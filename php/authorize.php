<?php

include "dbconnect.php";
session_start();

$username;
$password;
$row;
  
//Get username from HTML text input
if (isset($_POST['username']) && !empty($_POST["username"])) 
{
	$username = $_POST['username'];
	//eliminate exraneous whitespaces and dangerous characters
	$username = trim($con->real_escape_string($username)); 
}
else //no username entered
{
	  $username = "(Not entered)"; 
  }
  
//Get password from HTML text input
if (isset($_POST['password']) && !empty($_POST["password"])) 
{
	$password = $_POST['password']; 
	//eliminate dangerous characters
	$password = $con->real_escape_string($password);
}
else //no password entered
{
	$password = "(Not entered)";
}
 
  //query database for corresponding username and password
  $sql="SELECT * FROM admin WHERE username='$username' and password='$password'";
  $result=mysqli_query($con, $sql);

  // Mysql_num_row is counting table row
  $count=mysqli_num_rows($result);
  
  // if result matched $username and $email, table row must be 1 row
  if ($count==1) //successful login
  {
	$row = $result->fetch_assoc();
	//retrieve user role (teacher, principal, counselor, etc.)
	$_SESSION["RoleID"] = $row['roleID'];
	$roleID = $row['roleID'];
	$_SESSION["AdminID"]=$row['adminID'];
	$adminID = $row['adminID'];
	//set name to session variables
	$_SESSION["firstName"] =  $row['first_name'];
	$_SESSION["lastName"] =  $row['last_name'];
	
	//Retrieve role descriptive
	$sql="SELECT role FROM roles WHERE roleID='$roleID'";
	$result=mysqli_query($con, $sql);
	$row = $result->fetch_assoc();
	$_SESSION["role"] = $row['role'];
	
	if($roleID == 8){ //user is a teacher
		//query database for teacherID
		$sql="SELECT teacherID FROM teacher WHERE adminID='$adminID'";
		$result=mysqli_query($con, $sql);
		$row = $result->fetch_assoc();
		$_SESSION["teacherID"] = $row['teacherID'];
	}

	// redirect user to appropriate dashboard
	switch($roleID){
		
		case 1: //Administrative Assistant
			header("Location: ../adminAssistantDashboard.php");
			break;
		case 2:
		case 3: //Principal or Assistant Principal
			header("Location: ../schoolDashboard.php");
			break;
		case 4: //Nurse
			header("Location: ../nurseDashboard.php");
			break;
		case 5: //Counselor
			header("Location: ../counselorDashboard.php");
			break;
		case 6:
		case 7: //Superintendent or Assistant Superintendent
			header("Location: ../districtDashboard.php");
			break;
		case 8: //Teacher
			header("Location: ../teacherDashboard.php");
			break;
		default: //There has been an error
			header("Location: ../index.html");
			break;
	}
	
  }
  else {
	echo "<p>Login unsuccessful. </p>";
	$_SESSION["ReturnAlert"]= "null";
	// redirect user back to login page
    header("Location: ../index.html");
  }

?>