<?php

include "php/dbconnect.php";
session_start();
$roleID = $_SESSION["RoleID"];

switch($roleID){
		
		case 1: //Administrative Assistant
			echo "<nav>
				<ul>
					<li><a href = 'adminAssistantDashboard.php'>Home</a></li>
					<li><a href = 'faculty.php'>Faculty</a></li>
					<li><a href = 'staff.php'>Staff&emsp;</a></li>
					<li><a href = 'studentcontact.php'>Students</a></li>
					<li><a href = 'rosters.php'>Rosters</a></li>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
			</nav>";
			break;
		case 2:
		case 3: //Principal or Assistant Principal
			echo "<nav>
				<ul>
					<li><a href = 'schoolDashboard.php'>Home</a></li>
					<li><a href = 'faculty.php'>Faculty</a></li>
					<li><a href = 'staff.php'>Staff&emsp;</a></li>
					<li><a href = 'studentcontact.php'>Students</a></li>
					<li><a href = 'rosters.php'>Rosters</a></li>
					<li><a href = 'editemployees.php'>Staffing</a></li>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
			</nav>";
			break;
		case 4: //Nurse
			echo "<nav>
				<ul>
					<li><a href = 'nurseDashboard.php'>Home</a></li>
					<li><a href = 'allergies.php'>Allergies</a></li>
					<li><a href = 'studentcontact.php'>Students</a></li>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
				</nav>";
			break;
		case 5: //Counselor
			echo "<nav>
				<ul>
					<li><a href = 'counselorDashboard.php'>Home</a></li>
					<li><a href = 'studentcontact.php'>Students</a></li>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
				</nav>";
			break;
		case 6:
		case 7: //Superintendent or Assistant Superintendent
			echo "<nav>
				<ul>
					<li><a href = 'districtDashboard.php'>Home</a></li>
					<li><a href = 'faculty.php'>Faculty</a></li>
					<li><a href = 'staff.php'>Staff&emsp;</a></li>
					<li><a href = 'studentcontact.php'>Students</a></li>
					<li><a href = 'rosters.php'>Rosters</a></li>
					<li><a href = 'editemployees.php'>Staffing</a></li>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
			</nav>";
			break;
		case 8: //Teacher
			echo "<nav>
				<ul>
					<li><a href = 'teacherDashboard.php'>Home</a></li>
					<li><a href = 'studentcontact.php'>Students</a></li>
					<li><a href = 'roster.php'>Rosters</a></li>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
			</nav>";
			break;
		default: //There has been an error
			echo "<nav>
				<ul>
					<li><a href = 'logout.php'>LogOut</a></li>
				</ul>
			</nav>";
			break;
		
	}

?>