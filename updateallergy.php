<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Update Allergy Info</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		    
			if(isset($_POST["new"]))
			{
				unset($_POST["new"]);
			}
			if(isset($_POST["delete"]))
			{
				unset($_POST["delete"]);
			}
			if(isset($_SESSION["editstudent"])){
			    $thisstudent = $_SESSION["editstudent"];
			    $_SESSION["editstudent"] = $thisstudent;
			}
			
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Update Allergy Info</h3>";
			
			$thisstudent = $_POST["student"];
			$_SESSION["existing"] = "existing";
			
			//query for current info for this allergy
			$qry="SELECT first_name, last_name, allergy
			    FROM student INNER JOIN allergy ON student.studentID=allergy.studentID AND student.studentID=$thisstudent";
			$rslt=mysqli_query($con, $qry) or die($con->error);
			$rw= $rslt->fetch_assoc();
			$sname=$rw["first_name"] . " " . $rw["last_name"];
			$allergy=$rw["allergy"];
			
			echo "<form action='insertallergy.php' method='post'>
			        <input type='hidden' name='student' id='student' value=$thisstudent>
					<label for='student_name'>Student</label>
					<input type='text' id='student_name' name='student_name' readonly='readonly' value='$sname'>
					<label for='allergy'>Allergy</label>
					<input type='text' id='allergy' name='allergy' required='required' value='$allergy'>
					&emsp;&emsp;<button type='submit'>Save Changes</button>&emsp;&emsp;
					<button type='submit' name='delete' id='delete' value='delete'>Remove Allergy</button></form><br><br>
					&emsp;&emsp;<form action='allergies.php' style='display:inline;'><button type='submit'>Back to Allergies</button>
					</form>
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
