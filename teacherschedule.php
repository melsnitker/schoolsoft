<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Teacher Schedule</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_POST["teacherID"])){
			    $thisteacher=$_POST["teacherID"];
			    $_SESSION["teacherID"]=$thisteacher;
			}elseif(isset($_SESSION["teacherID"])){
			    $thisteacher=$_SESSION["teacherID"];
			    $_SESSION["teacherID"]=$thisteacher;
			}
			if(isset($_SESSION["new"])){
			    unset($_SESSION["new"]);
			}
			if(isset($_SESSION["existing"])){
			    unset($_SESSION["existing"]);
			}
			if(isset($_POST["new"])){
			    unset($_POST["new"]);
			}
			if(isset($_POST["existing"])){
			    unset($_POST["existing"]);
			}
			
			//query database for teacher name
			$sql="SELECT first_name, last_name FROM teacher_info WHERE teacherID=$thisteacher";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row=$result->fetch_assoc();
			$tname=$row['first_name'] . ' ' . $row['last_name'];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Schedule for $tname</h3>";
			
			//query for current schedule
			$sql="SELECT class_period, subject FROM teaches WHERE teacherID=$thisteacher ORDER BY class_period;";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			//query for number of class periods at this school
			$sql2="SELECT max(class_period) FROM class_periods";
			$result2=mysqli_query($con, $sql2) or die($con->error);
			$row2=$result2->fetch_assoc();
			$numberofperiods=$row2["max(class_period)"];
			
			echo "<div>";
				//Header of table
				echo "<table>
					<tr>
						<th>Class Period</th>
						<th>Subject</th>
						<th></th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						
						$period = $row['class_period'];
						$subject = $row['subject'];
						
						echo "<tr><form action='addteacherclass.php' method='post'>";
						echo "<td>$period</td>
						    <td>$subject</td>
						    <td><button type='submit' name='classperiod' value='$period'>Change</button></td>
						    <input type='hidden' name='existing' id='existing' value='Change'></form></tr>"; 
					  }
					  
					  while($period < $numberofperiods){
						  $period++;
						  echo "<tr><form action='addteacherclass.php' method='post'>
						      <td>$period</td>
						      <td>Choose a Subject</td>
						      <td><button type='submit' name='classperiod' value='$period'>Add</button></td>
						      <input type='hidden' name='new' id='new' value='Add'></form></tr>";
					  }
					  
				echo "</table>";
				echo "<br><br>"; 
				echo "<form action='teacherschedules.php' style='display:inline;'><button type='submit'>Back to Teacher Scheduling</button></form>";
			echo "</div>";
		?>
		
	</div>
	<br>

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
