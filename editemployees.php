<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Employee Management</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		
		    if(isset($_POST["delete"]))
			{
				unset($_POST["delete"]);
			}
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Employees</h3>";
			
			echo "<form action='newemployee.php' style='display:inline;'><button type='submit'>New Employee</button></form><br><br>";
			
			//query database for faculty information
			$sql="SELECT admin.roleID, adminID, first_name, last_name, role FROM admin INNER JOIN roles ON admin.roleID=roles.roleID ORDER BY admin.roleID";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>
			<form action='updateemployee.php' method='post'>";
				//Header of table
				echo "<table>
					<tr>
						<th>Staff Member Name</th>
						<th>Position</th>
						<th></th>
						<th></th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$thisname = $row['first_name'] . ' ' . $row['last_name'];
						$adminID=$row['adminID'];
						$thisrole=$row['role'];
						echo "<tr><td>$thisname</td><td>$thisrole</td>
						      <td><button type='submit' name='adminID' value='$adminID'>Edit</button></td>
							  <td><button type='submit' id='delete' name='delete' value='$adminID'>Separate</button></td></tr>"; 
					  }
				echo "</table>";
			echo "</form></div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
