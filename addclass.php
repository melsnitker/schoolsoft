<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Course Registration</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION["studentID"])){
			    $studentID=$_SESSION["studentID"];
			    $_SESSION["studentID"] = $studentID;
			}
			if(isset($_SESSION["classperiod"])){
				unset($_SESSION["classperiod"]);
			}
			if(isset($_POST["classperiod"])){
				$thisperiod=$_POST["classperiod"];
				$_SESSION["classperiod"] = $thisperiod;
			}
			if(isset($_POST["existing"])){
				$_SESSION["existing"] = $_POST["existing"];
			}
			elseif(isset($_POST["new"])){
				$_SESSION["new"] = $_POST["new"];
			}
			
			//query database for student name
			$sql="SELECT first_name, last_name FROM student WHERE studentID=$studentID";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row=$result->fetch_assoc();
			$sname=$row['first_name'] . ' ' . $row['last_name'];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Schedule for $sname - Add Class</h3>";
			
			//query for subjects available this period
			$sql="SELECT subject, teacherID FROM teaches WHERE class_period=$thisperiod";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>";
				echo "<form action='updateschedule.php' method='post' style='display:inline;'>";
				//Header of table
				echo "<table>
				    <tr><th>Class Period</th><th>Subject</th></tr>
					<tr><td>$thisperiod</td>
					<td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<select name='teacher' id='teacher' style='margin-left:230px;'>";
					
					while ($row = $result->fetch_assoc()){
						$thissubject = $row['subject'];
						$teachID = $row['teacherID'];
						echo "<option value=$teachID>$thissubject</option>";
					  }
					  
		        echo "</select></td><tr>";
				echo "</table>";
				echo "<br><br>";
				echo "<button type='submit' id='editstudent' name='editstudent' value=$studentID>Add Class</button></form>&emsp;&emsp;
				     <form action='editcourses.php' style='display:inline;'><button type='submit'>Back to Scheduling</button></form>";
			echo "</div>";
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
