<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Faculty Contact Information</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			//query database for teacher information
			$sql="SELECT first_name, last_name, phone FROM admin INNER JOIN teacher ON admin.adminID=teacher.adminID";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>";
				//Header of table
				echo "<table>
					<tr>
						<th>Teacher Name</th>
						<th>Phone Number</th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$name = $row['first_name'] . ' ' . $row['last_name'];
						$phone = $row['phone'];
						echo "<tr><td>$name</td><td>$phone</td></tr>"; 
					  }
				echo "</table>";
			echo "</div>";
		?>

		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>