<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Update Parent Info</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		    if(isset($_SESSION["editparent"]))
			{
				unset($_SESSION["editparent"]);
			}
			if(isset($_POST["new"]))
			{
				unset($_POST["new"]);
			}
			if(isset($_POST["delete"]))
			{
				unset($_POST["delete"]);
			}
			$thisstudent = $_SESSION["editstudent"];
			$_SESSION["editstudent"] = $thisstudent;
			if(isset($_POST["editparent"])){
			    $thisparent = $_POST["editparent"];
			    $_SESSION["editparent"] = $thisparent;
		    }
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Update Parent Info</h3>";
			
			//query for current info for this parent
			$qry="SELECT p_first_name, p_last_name, email, phone_number, type, relationship
			    FROM student_parent_info INNER JOIN parent_to_student ON student_parent_info.parentID=parent_to_student.parentID AND parent_to_student.parentID=" . $thisparent;
			$rslt=mysqli_query($con, $qry) or die($con->error);
			$rw= $rslt->fetch_assoc();
			
			
			echo "<form action='insertparent.php' method='post'>
					<label for='p_first_name'>First Name</label>
					<input type='text' id='p_first_name' name='p_first_name' required='required' value=" . $rw['p_first_name'] . ">
					<label for='p_last_name'>Last Name</label>
					<input type='text' id='p_last_name' name='p_last_name' required='required' value=" . $rw['p_last_name'] . ">
					<label for='email'>Email</label>
					<input type='email' id='email' name='email'  value=" . $rw['email'] . ">
					<label for='p_phone'>Phone</label>
					<input type='text' id='p_phone' name='p_phone' required='required' value=" . $rw['phone_number'] . ">
					<label for='phonetype'>Phone Type</label>
					<input type='text' id='phonetype' name='phonetype' required='required' value=" . $rw['type'] . ">
					<label for='relationship'>Relationship to Student</label>
					<input type='text' id='relationship' name='relationship' required='required' value=" . $rw['relationship'] . "><br><br>
					&emsp;&emsp;<button type='submit'>Save Changes</button>&emsp;&emsp;
					<button type='submit' name='delete' id='delete' value='delete'>Remove Parent</button></form><br><br>
					&emsp;&emsp;<form action='studentcontact.php' style='display:inline;'><button type='submit'>Back to Students</button>
					</form>
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
