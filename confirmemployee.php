<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Employee</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Employees - Confirm New Hire</h3>";
			
			$newfirstname=$_POST["first_name"];
			$newlastname=$_POST["last_name"];
			$newphone=$_POST["phone"];
			$newroleid=$_POST["role"];
			$newusername=$_POST["username"];
			$newpassword=$_POST["password"];
			
			if(isset($_POST["gradelevel"]))
				$gradelevel = $_POST["gradelevel"];
			if(isset($_POST["roomnumber"]))
				$roomnumber = $_POST["roomnumber"];
			
			
			//insert new employee
			$sql="INSERT INTO admin (first_name, last_name, roleID, password, username, phone)
				VALUES ('" .$newfirstname. "', '" .$newlastname. "', " .$newroleid. ", '" .$newpassword. "', '" .$newusername. "', '" .$newphone."')";
			
			$result=mysqli_query($con, $sql) or die($con->error);
			
			//if new employee is a teacher, insert into teacher table as well
			if($newroleid==8){
			$sql="SELECT adminID FROM admin WHERE first_name='$newfirstname' AND last_name='$newlastname' AND phone='$newphone'";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row=$result->fetch_assoc();
			$thisid=$row["adminID"];
			
			$sql="INSERT INTO teacher (grade_level, classroom, adminID)
				VALUES (" .$gradelevel. ", " .$roomnumber. ", " .$thisid.")";
			$result=mysqli_query($con, $sql) or die($con->error);}
			
			//query database for role label
			$sql="SELECT role FROM roles WHERE roleID=$newroleid";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row = $result->fetch_assoc();
			$position = $row['role'];
			
			echo "<div>";
				//Header of table
				echo "<table>
					<tr>
						<th>Name</th>
						<th>Phone Number</th>
						<th>Position</th>
						<th>Username</th>
						<th>Password</th>
					</tr>
					<tr>
						<td>$newfirstname  $newlastname</td>
						<td>$newphone</td>
						<td>$position</td>
						<td>$newusername</td>
						<td>$newpassword</td>
					</tr>";
	
				echo "</table><br>";
				echo "<form action='editemployees.php' style='display:inline;'><button type='submit'>Return to Staff List</button></form>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
