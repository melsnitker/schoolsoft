<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft - Student Contacts</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			if(isset($_POST["studentID"])){
			    $studentID = $_POST["studentID"];
			    $_SESSION["editstudent"] = $studentID;
			}
			if(isset($_POST["new"])){
				unset($_POST["new"]);
			}
	        if(isset($_POST["delete"])){
				unset($_POST["delete"]);
			}
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			//query database for student name
			$sql2="SELECT first_name, last_name FROM student WHERE studentID=$studentID";
			$result=mysqli_query($con, $sql2) or die($con->error);
			$row = $result->fetch_assoc();
			$sname = $row['first_name'] . ' ' . $row['last_name'];
			
			echo "<h3>Viewing Contact Information for " . $sname . "</h3>";
			
			//query database for parent information
			$sql3="SELECT parentID, p_first_name, p_last_name, phone_number, type, email FROM student_parent_info WHERE studentID=$studentID";
			$result=mysqli_query($con, $sql3) or die($con->error);		
			
			echo "<div>";
				//Header of table
				echo "<table><caption>Parent Contact Information</caption>
					<tr>
					    <th><form action='newparentinfo.php' style='display:inline;'><button type='submit'>Add</button>
					        <input type='hidden' name='existing' id='existing' value='existing'></form></th>
						<th>Parent Name</th>
						<th>Phone Number</th>
						<th>Phone Type</th>
						<th>Email</th>
					</tr>";
					
					while ($row = $result->fetch_assoc()){
						$parname = $row['p_first_name'] . ' ' . $row['p_last_name'];
						$phone = $row['phone_number'];
						$type = $row['type'];
						$email = $row['email'];
						$parid = $row['parentID'];
						
						echo "<form method='post' action='updateparent.php'><tr>
						    <td><button type='submit' name='editparent' id='editparent' value =$parid>Edit</button></td>
						    <td>$parname</td>
						    <td>$phone</td>
						    <td>$type</td>
						    <td>$email</td></tr></form>";
					}
					echo "</table><br><br><table><caption>Emergency Contact Information</caption><tr>
					    <th><form action='newemergencyinfo.php' style='display:inline;'><button type='submit'>Add</button>
					        <input type='hidden' name='existing' id='existing' value='existing'></form></th>
						<th>Contact Name</th>
						<th>Phone Number</th>
						<th> </th>
						<th> </th>
					</tr>";
					
					//query database for other emergency contact information
					$qry="SELECT contactID, first_name, last_name, phone_number FROM contact_info WHERE studentID=$studentID";
					$rslt=mysqli_query($con, $qry) or die($con->error);
					
					while ($row = $rslt->fetch_assoc()){
						$cname = $row['first_name'] . ' ' . $row['last_name'];
						$phone = $row['phone_number'];
						$cid = $row['contactID'];
						echo "<form method='post' action='updateemergency.php'><tr><td><button type='submit' name='editcontact' id='editcontact' value =$cid>Edit</button></td>
						    <td>$cname</td>
						    <td>$phone</td>
						    <td> </td>
						    <td> </td></tr></form>"; 
					}
					echo "</table><br><br><table><caption>Physician Contact Information</caption><tr>
					    <th><form action='newpcpinfo.php' style='display:inline;'><button type='submit'>Add</button>
					        <input type='hidden' name='existing' id='existing' value='existing'></form></th>
						<th>Physician Name</th>
						<th>Phone Number</th>
						<th> </th>
						<th> </th>
					</tr>";
					
					//query database for primary care physician information
					$qry="SELECT pcID, first_name, last_name, phone_number FROM primary_care_contact WHERE studentID=$studentID";
					$rslt=mysqli_query($con, $qry) or die($con->error);
					
					while ($row = $rslt->fetch_assoc()){
						$name = $row['first_name'] . ' ' . $row['last_name'];
						$phone = $row['phone_number'];
						$pcid = $row['pcID'];
						echo "<form method='post' action='updatepcp.php'><tr><td><button type='submit' name='editpcp' id='editpcp' value =$pcid>Edit</button></td>
						    <td>$name</td>
						    <td>$phone</td>
						    <td> </td>
						    <td> </td></tr></form>"; 
					}
				echo "</table><br><br>";
			echo "<form action='studentcontact.php' style='display:inline;'><button type='submit'>Back to Student Contacts</button></form>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
