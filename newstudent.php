<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Student</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		
		    if(isset($_SESSION["newstudent"]))
		    {
				unset($_SESSION["newstudent"]);
			}
			if(isset($_SESSION["existing"]))
		    {
				unset($_SESSION["existing"]);
			}
			if(isset($_POST["existing"]))
		    {
				unset($_POST["existing"]);
			}
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Add New Student</h3>";
			
			//query database for teachers
			$sql="SELECT teacherID, first_name, last_name FROM teacher_info";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<form action='newparentinfo.php' method='post' style='display:inline;'>
			        <input type='hidden' name='new' id='new' value ='new'>
					<label for='s_first_name'>First Name</label>
					<input type='text' id='s_first_name' name='s_first_name' required='required'>
					<label for='middleinitial'>Middle Initial</label>
					<input type='text' id='middleinitial' name='middleinitial' required='required'>
					<label for='last_name'>Last Name</label>
					<input type='text' id='last_name' name='last_name' required='required'>
					<label for='gradelevel'>Grade Level</label>
					<input type='number' id='gradelevel' name='gradelevel' required='required'>
					<label for='homeroomteacher'>Homeroom Teacher</label>
					<select id='homeroomteacher' name='homeroomteacher' required='required'>";
					while($row=$result->fetch_assoc()){
						$thisid=$row["teacherID"];
						$thisname=$row["first_name"] . " " . $row["last_name"];
 						echo "<option value=" .$thisid.">".$thisname."</option>";
					}
					echo "</select><br>
					
					&emsp;&emsp;<button type='submit'>Next</button></form>
					&emsp;&emsp;<form action='editstudents.php' style='display:inline;'><button type='submit'>Back to Students</button></form>
					
					";
			
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
