<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Physician</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			if(isset($_SESSION["newstudent"])){
			    $thisstudent = $_SESSION["newstudent"];
			    $_SESSION["newstudent"] = $thisstudent;
		    } elseif(isset($_SESSION["editstudent"])){
				$thisstudent = $_SESSION["editstudent"];
				$_SESSION["editstudent"] = $thisstudent;
				
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - New Physician Contact</h3>";
			
			$physicianfirstname=$_POST["first_name"];
			$physicianlastname=$_POST["last_name"];
			$phone=$_POST["phone"];
			
			if(isset($_POST["new"])){
			    //insert new pcp info
			    $sql="INSERT INTO primary_care_contact (studentID, first_name, last_name, phone_number)
				    VALUES ($thisstudent, '$physicianfirstname', '$physicianlastname', '$phone')";
			    $result=mysqli_query($con, $sql) or die($con->error);
			}elseif(isset($_POST["delete"])){
			    $thispcp = $_SESSION["editpcp"];
				$_SESSION["editpcp"] = $thispcp;
			
			    //delete pcp
			    $sql="DELETE FROM primary_care_contact WHERE pcID=$thispcp";
			    $result=mysqli_query($con, $sql) or die($con->error);
			
			}else{
				$thispcp = $_SESSION["editpcp"];
				$_SESSION["editpcp"] = $thispcp;
				
				//update physician info
				$sql="UPDATE primary_care_contact SET first_name='$physicianfirstname', last_name='$physicianlastname', phone_number='$phone'
				    WHERE pcID=" . $thispcp;
			    $result=mysqli_query($con, $sql) or die($con->error);
			}
			
			echo "&emsp;&emsp;<form action='newpcpinfo.php' style='display:inline;'><button type='submit'>Add Another Physician</button></form><br><br>";
			echo "&emsp;&emsp;<form action='editstudents.php' style='display:inline;'><button type='submit'>Manage Students</button></form>";
			echo "&emsp;&emsp;<form action='studentcontact.php' style='display:inline;'><button type='submit'>Student Contacts</button></form>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
