function link_dashboard(int role) {
	switch(role){
		
		case 1:   //Administrative Assistant
			window.location.href = "adminAssistantDashboard.html";
			break;
		case 2:   //Principal or Assistant Principal
		case 3:
			window.location.href = "schoolDashboard.html";
			break;
		case 4:   //Nurse
			window.location.href = "nurseDashboard.html";
			break;
		case 5:   //Counselor
			window.location.href = "counselorDashboard.html";
			break;
		case 6:   //Superintendent or Superintendent
		case 7:
			window.location.href = "districtDashboard.html";
			break;
		case 8:   //Teacher
			window.location.href = "teacherDashboard.html";
			break;
		default:  //There has been an error
			window.location.href = "index.html";
		
	}
	
}