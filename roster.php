<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft - Rosters</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "192" width = "192" class = "floatright">
    <br>

	<div>
		<?php
			
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			//unset relevant roster-viewing variables
			if(isset($_POST["classperiod"])){
				unset($_POST["classperiod"]);
			}
			if(isset($_SESSION["classperiod"])){
				unset($_SESSION["classperiod"]);
			}
			
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			//Check if we arrived at this page as a teacher or admin
			if ($roleID != 8) //admin
			{
				if(isset($_POST["teacherID"])){
				    $teacherID = $_POST["teacherID"]; 
			    }
				
				//query database for teacher name
				$sql="SELECT first_name, last_name FROM teacher_info WHERE teacherID=$teacherID";
				$result=mysqli_query($con, $sql);
				$row = $result->fetch_assoc();
				$tname = $row['first_name'] . ' ' . $row['last_name'];
				
				$_SESSION["teacherbyadmin"] = $teacherID;
				echo "<h2>Viewing Rosters for $tname</h2>";
				
			}
			else //teacher
			{
				if(isset($_SESSION["teacherID"])){
				    $teacherID = $_SESSION["teacherID"];
				    $_SESSION["teacherID"] = $teacherID;
				}
			}
			
			//query for number of class periods at this school
			$sql="SELECT max(class_period) FROM class_periods";
			$result=mysqli_query($con, $sql);
			$row = $result->fetch_assoc();
			$maxperiod = $row["max(class_period)"];
			
			echo "<form action='coursegrades.php' method='post'>";
			
			for($i=1; $i<=$maxperiod; $i++){
				echo "<button type='submit' name='classperiod' value=$i>Period $i</button>&emsp;&emsp;";
				if($i%2==0){
					echo "<br><br>";
				}
			}
			
		echo "</form><br><br>";
		if($roleID == 8){
		    echo "<form action='teacherDashboard.php' style='display:inline;'><button type='submit'>Back to Home</button></form>";
        } else{
			echo "<form action='rosters.php' style='display:inline;'><button type='submit'>Back to Rosters</button></form>";
		}			
			
		?>
		
	</div>
	<br>

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
