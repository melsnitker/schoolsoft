<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Emergency Contact</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			if(isset($_SESSION["newstudent"])){
			    $thisstudent = $_SESSION["newstudent"];
			    $_SESSION["newstudent"] = $thisstudent;
		    } elseif(isset($_SESSION["editstudent"])){
				$thisstudent = $_SESSION["editstudent"];
				$_SESSION["editstudent"] = $thisstudent;
				
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - New Emergency Contact</h3>";
			
			$contactfirstname=$_POST["first_name"];
			$contactlastname=$_POST["last_name"];
			$phone=$_POST["phone"];
			
			if(isset($_POST["new"])){
			    //insert new contact info
			    $sql="INSERT INTO emergency_contact (first_name, last_name, phone_number)
				    VALUES ('$contactfirstname', '$contactlastname', '$phone')";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    $newcontactid = mysqli_insert_id($con);
			
			    //insert into student_emergency
			    $sql="INSERT INTO student_emergency (contactID, studentID)
				    VALUES ($newcontactid, $thisstudent)";
			    $result=mysqli_query($con, $sql) or die($con->error);
		    }elseif(isset($_POST["delete"])){
			
			    $thiscontact = $_SESSION["editcontact"];
				$_SESSION["editcontact"] = $thiscontact;
			
			    //delete contact
			    $sql="DELETE FROM emergency_contact WHERE contactId=$thiscontact";
			    $result=mysqli_query($con, $sql) or die($con->error);
			
			}else{
				
				$thiscontact = $_SESSION["editcontact"];
				$_SESSION["editcontact"] = $thiscontact;
				
				//update contact info
				$sql="UPDATE emergency_contact SET first_name='$contactfirstname', last_name='$contactlastname', phone_number='$phone'
				    WHERE contactID=" . $thiscontact;
			    $result=mysqli_query($con, $sql) or die($con->error);
				
			}
			
			
			echo "<form action='newpcpinfo.php' method='post' style='display:inline;'>
					&emsp;&emsp;<button type='submit'>Add Physician</button></form>&emsp;&emsp;
					<form action='newemergencyinfo.php' style='display:inline;'><button type='submit'>Add Another Contact</button></form><br><br>";
			echo "&emsp;&emsp;<form action='editstudents.php' style='display:inline;'><button type='submit'>Manage Students</button></form>";
			echo "&emsp;&emsp;<form action='studentcontact.php' style='display:inline;'><button type='submit'>Student Contacts</button></form>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
