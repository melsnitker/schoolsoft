<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Update Teacher Schedule</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION["teacherID"])){
				$thisteacher=$_SESSION["teacherID"];
			    $_SESSION["teacherID"] = $thisteacher;
			}
			if(isset($_SESSION["classperiod"])){
				$thisperiod = $_SESSION["classperiod"];
				$_SESSION["classperiod"] = $thisperiod;
			}
			if(isset($_POST["subject"])){
				$thissubject=$_POST["subject"];
			}
			
			//query database for teacher name
			$sql="SELECT first_name, last_name FROM teacher_info WHERE teacherID=$thisteacher";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row=$result->fetch_assoc();
			$tname=$row['first_name'] . ' ' . $row['last_name'];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Teacher Schedule - $tname</h3>";
			
			if(isset($_SESSION["existing"])){
				
				//update schedule for this period
				$sql="UPDATE teaches SET subject='$thissubject'
				    WHERE teacherID=$thisteacher AND class_period=$thisperiod";
			    $result=mysqli_query($con, $sql) or die($con->error);
				
			}elseif(isset($_SESSION["new"])){
				
				//insert class for this period
				$sql="INSERT INTO teaches (teacherID, class_period, subject)
				    VALUES ($thisteacher, $thisperiod, '$thissubject')";  
				$result=mysqli_query($con, $sql) or die($con->error);
				
			}
				
			echo "<h4>Schedule successfully updated.</h4>";
			
			echo "&emsp;&emsp;<form action='teacherschedule.php' style='display:inline;'><button type='submit'>Schedule for $tname</button></form><br><br>";
			echo "&emsp;&emsp;<form action='teacherschedules.php' style='display:inline;'><button type='submit'>Manage Teacher Schedules</button></form>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
