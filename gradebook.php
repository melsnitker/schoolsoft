<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Gradebook</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION['classperiod'])){
			    $class_period = $_SESSION['classperiod'];
			    $_SESSION['classperiod'] = $class_period;
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			//Check if we arrived at this page as a teacher or admin
			if ($roleID != 8) //admin
			{
				if(isset($_SESSION["teacherbyadmin"])){
				    $teacherID = $_SESSION["teacherbyadmin"];
				    $_SESSION["teacherbyadmin"] = $teacherID;
				}
				//query database for teacher name
				$sql="SELECT adminID FROM teacher WHERE teacherID='$teacherID'";
				$result=mysqli_query($con, $sql);
				$row = $result->fetch_assoc();
				$id = $row['adminID'];
				
				$sql="SELECT first_name, last_name FROM admin WHERE adminID='$id'";
				$result=mysqli_query($con, $sql);
				$row = $result->fetch_assoc();
				$tname = $row['first_name'] . ' ' . $row['last_name'];
				
				echo "<h2>Roster - Period " . $class_period .  " - " . $tname . "</h2>";
				
				//query database for corresponding class roster
				$sql="SELECT studentID, course_grade FROM takes WHERE class_period='$class_period' and teacherID='$teacherID'";
				$result=mysqli_query($con, $sql) or die($con->error);
			}
			else //teacher
			{
				if(isset($_SESSION["teacherID"])){
				    $teacherID = $_SESSION["teacherID"];
				    $_SESSION["teacherID"] = $teacherID;
				}
				
				//query for subject
				$sql = "SELECT subject FROM teaches WHERE teacherID=$teacherID AND class_period=$class_period";
				$result=mysqli_query($con, $sql) or die($con->error);
				$row = $result->fetch_assoc();
				$thissubject = $row["subject"];
				
				echo "<h2>Period " . $class_period .  " Gradebook - $thissubject</h2>";
				
				//query database for assignments for ths class
				$sql = "SELECT DISTINCT assignment_name from assignment WHERE teacherID=$teacherID AND class_period=$class_period";
				$result = mysqli_query($con, $sql) or die($con->error);
				
			}
			
			echo "<div>";
			//Header of table
			echo "<table>
					<tr>
					    <th>Assignment Name</th>
						<th>Student ID No.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Assignment Grade</th>
						<th></th>
					</tr>";
			
			while ($row = $result->fetch_assoc()){
				
				$thisassignment = $row["assignment_name"];
				
				echo "<tr><th>$thisassignment</th>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>
				<td><form action='deleteassignment.php' method='post'>
				    <button type='submit' name='deleteassignment' value='$thisassignment' class='gradebookbutton' onclick=\"return confirm('Are you sure?');\">Delete Assignment</button>
				    <input type='hidden' name='teacher' value='$teacherID'>
				    <input type='hidden' name='period' value='$class_period'></form></td></tr>";
				
				//query the gradebook for this assignment
				$query = "SELECT studentID, grade FROM gradebook WHERE teacherID=$teacherID AND assignment_name='$thisassignment' AND class_period=$class_period";
				$rslt=mysqli_query($con, $query) or die($con->error);
				while($rw = $rslt->fetch_assoc()){
					
					$sID = $rw["studentID"];
					$thisstudent = "student" . $sID;
					$grade = $rw["grade"];
					
					$sql2 = "SELECT first_name, last_name FROM student WHERE studentID=$sID";
					$rslt2=mysqli_query($con, $sql2) or die($con->error);
					$rw2 = $rslt2->fetch_assoc();
					$sfname = $rw2["first_name"];
					$slname = $rw2["last_name"];
					
					echo "<form action='updategrades.php' method='post'><tr><td></td>
					    <td>$sID</td>
					    <td>$sfname</td>
					    <td>$slname</td>
					    <td><input type='number' name='student[$sID]' value='$grade'></td><td></td></tr>";
					
				}
				echo "<tr><td></td>
				    <td></td>
				    <td></td>
				    <td></td>
				    <td></td>
				    <td><button type='submit' name='assignment' class='gradebookbutton' value='$thisassignment'>Save Grades</button></td></tr></form>";
				
			}

			echo "</table>";
			echo "</div>";
			
			echo "</div>";
			echo "<br><br>";
			
			$_SESSION["teacherID"] = $teacherID;
			$_SESSION["firstName"] = $fname;
			$_SESSION["lastName"] = $lname;
			$_SESSION["role"] = $role;
			echo "<form action='addassignment.php' style='display:inline;'><button type='submit'>Add Assignment</button></form>&emsp;&emsp;";
			echo "<form action='roster.php' style='display:inline;'><button type='submit'>Back to Rosters</button></form>";
			
			
		?>
		
		
	
	
	

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
