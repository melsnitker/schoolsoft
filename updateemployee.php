<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Employee</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		
			if(isset($_SESSION["editadmin"]))
			{
				unset($_SESSION["editadmin"]);
			}
			
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Employees - Update Information</h3>";
			
			if(isset($_POST["delete"])){
				
				$thisadminid = $_POST["delete"];
			    $_SESSION["editadmin"] = $thisadminid;
			    //query database for this employee's name
				$qry="SELECT first_name, last_name FROM admin WHERE adminID=" . $thisadminid;
			    $rslt=mysqli_query($con, $qry) or die($con->error);
			    $rw= $rslt->fetch_assoc();
			    $thisname = $rw["first_name"] . ' ' . $rw["last_name"];
			    $_SESSION["editname"] = $thisname;
			    
			    echo "<h3>Are you sure you want to do this?</h3>
			          <h4>Separate " . $thisname . "?</h4>
			          <form action='updatecomplete.php' method='post' style='display:inline;'>
			          <button type='submit' name='delete' id='delete' value='$thisadminid'>Yes, Separate</button></form>&emsp;&emsp;
			          <form action='editemployees.php' style='display:inline;'><button type='submit'>Back</button></form>";
			    
				
			}else{
			
			$thisadminid = $_POST["adminID"];
			$_SESSION["editadmin"] = $thisadminid;
			//query database for role labels
			$sql="SELECT * FROM roles";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			//query database for this employee's current information
			$qry="SELECT first_name, last_name, admin.roleID, password, username, phone, role FROM admin INNER JOIN roles ON admin.roleID=roles.roleID AND adminID=" . $thisadminid;
			$rslt=mysqli_query($con, $qry) or die($con->error);
			$rw= $rslt->fetch_assoc();
			$currentrole=$rw['role'];
			
			echo "<form action='updatecomplete.php' method='post'>
					<label for='first_name'>First Name</label>
					<input type='text' id='first_name' name='first_name' value='" . $rw['first_name'] . "'>
					<label for='last_name'>Last Name</label>
					<input type='text' id='last_name' name='last_name' value='" . $rw['last_name'] . "'>
					<label for='phone'>Phone</label>
					<input type='text' id='phone' name='phone' value='" . $rw['phone'] . "'>
					<label for='role'>Position</label>
					<select id='role' name='role'>";
					echo "<option value=" .$rw['roleID'] . ">".$currentrole."</option>";
					while ($row = $result->fetch_assoc()){
						$thisroleid=$row['roleID'];
						$thisrole=$row['role'];
						echo "<option value=$thisroleid>$thisrole</option>";
					}
					echo "</select>
					<label for='username'>Username</label>
					<input type='text' id='username' name='username' value='" . $rw['username'] . "'>
					<label for='password'>Password</label>
					<input type='text' id='password' name='password' value='" . $rw['password'] . "'>";
					echo "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button type='submit' id='update' name='update' value='update'>Update</button></form>
					&emsp;&emsp;<form action='editemployees.php' style='display:inline;'><button type='submit'>Back</button>
					</form>
					";
			}
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
