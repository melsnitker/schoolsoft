<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Update Physician Info</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		    if(isset($_SESSION["editpcp"]))
			{
				unset($_SESSION["editpcp"]);
			}
			if(isset($_POST["new"]))
			{
				unset($_POST["new"]);
			}
			if(isset($_SESSION["editstudent"])){
			    $thisstudent = $_SESSION["editstudent"];
			    $_SESSION["editstudent"] = $thisstudent;
			}
			if(isset($_POST["editpcp"])){
			    $thispcp = $_POST["editpcp"];
			    $_SESSION["editpcp"] = $thispcp;
		    }
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Update Physician Info</h3>";
			
			//query for current info for this physician
			$qry="SELECT first_name, last_name, phone_number
			    FROM primary_care_contact WHERE pcID=$thispcp AND studentID=$thisstudent";
			$rslt=mysqli_query($con, $qry) or die($con->error);
			$rw= $rslt->fetch_assoc();
			
			
			echo "<form action='insertpcp.php' method='post'>
					<label for='first_name'>First Name</label>
					<input type='text' id='first_name' name='first_name' required='required' value=" . $rw['first_name'] . ">
					<label for='last_name'>Last Name</label>
					<input type='text' id='last_name' name='last_name' required='required' value=" . $rw['last_name'] . ">
					<label for='phone'>Phone</label>
					<input type='text' id='phone' name='phone' required='required' value=" . $rw['phone_number'] . "><br><br>
					&emsp;&emsp;<button type='submit'>Save Changes</button>&emsp;&emsp;
					<button type='submit' name='delete' id='delete' value='delete'>Remove Physician</button></form><br><br>
					&emsp;&emsp;<form action='studentcontact.php' style='display:inline;'><button type='submit'>Back to Students</button>
					</form>
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
