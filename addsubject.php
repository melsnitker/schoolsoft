<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Subject</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_POST["subject"])){
				unset($_POST["subject"]);
			}
			if(isset($_SESSION["subject"])){
				unset($_SESSION["subject"]);
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Add a Course Subject</h3>";
			
		    //query for currently offered subjects
		    $sql = "SELECT DISTINCT subject FROM subjects";
		    $result=mysqli_query($con, $sql) or die($con->error);
		    echo "<table><tr><th>Subjects Offered</th></tr>";
		    while($row = $result->fetch_assoc()){
				$thissubject = $row["subject"];
				echo "<tr><td>$thissubject</td></tr>";
			}
			echo "</table><br><br>";
			
			echo "<form action='insertsubject.php' method='post' style='display:inline;'>
			        <input type='hidden' name='new' id='new' value ='new'>
					<label for='subject'>Subject to Add</label>
					<input type='text' id='subject' name='subject' required='required'><br>
					&emsp;&emsp;<button type='submit'>Save</button></form>&emsp;&emsp;
					<form action='schoolDashboard.php' style='display:inline;'><button type='submit'>Back to Home</button></form>
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
