<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Allergies</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			if(isset($_POST["delete"])){
				unset($_POST["delete"]);
			}
			if(isset($_SESSION["delete"])){
				unset($_SESSION["delete"]);
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Student Allergies</h3>";
			
			echo "<form action='newallergy.php' style='display:inline;'><button type='submit'>New Allergy</button></form><br><br>";
			
			//query database for allergy information
			$sql="SELECT student.studentID, first_name, last_name, allergy FROM student INNER JOIN allergy ON student.studentID=allergy.studentID ORDER BY last_name";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>";
				//Header of table
				echo "<table>
					<tr>
					    <th> </th>
						<th>Student Name</th>
						<th>Allergy</th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$allergy = $row['allergy'];
						$name=$row['first_name'] . ' ' . $row['last_name'];
						$sid=$row["studentID"];
						echo "<form method='post' action='updateallergy.php' style='display:inline;'>
						    <tr><td><button type='submit' name='student' id='student' value='$sid'>Edit</button>
						    <input type='hidden' name='existing' id='existing' value='existing'></td>
						    <td>$name</td><td>$allergy</td></tr></form>";
					  }
				echo "</table>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
