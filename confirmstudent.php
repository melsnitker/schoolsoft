<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Employee</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Confirm New Student</h3>";
			
			$newfirstname=$_POST["first_name"];
			$newlastname=$_POST["last_name"];
			$newmiddle=$_POST["middleinitial"];
			$newgradelevel=$_POST["gradelevel"];
			$newhomeroom=$_POST["homeroomteacher"];
			
			//insert new student
			$sql="INSERT INTO student (first_name, last_name, middle_initial, grade_level, homeroom_teacher)
				VALUES ('" .$newfirstname. "', '" .$newlastname. "', '" .$newmiddle. "', " .$newgradelevel. ", '" .$newhomeroom."')";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			//insert into homeroom roster
			$sql="SELECT studentID FROM student WHERE first_name='$newfirstname' AND last_name='$newlastname' AND middle_initial='$newmiddle'";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row=$result->fetch_assoc();
			$thisid=$row["studentID"];
			$sql="INSERT INTO takes (studentID, class_period, teacherID, subject, course_grade)
				VALUES ($thisid, 1, $newhomeroom, 'Homeroom', 95)";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			
			//query teacher name
			$sql="SELECT first_name, last_name FROM admin INNER JOIN teacher ON admin.adminID=teacher.adminID AND teacherID=$newhomeroom";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row = $result->fetch_assoc();
			$tname = $row["first_name"] . " " . $row["last_name"];
			
			echo "<div>";
				//Header of table
				echo "<table>
					<tr>
						<th>Name</th>
						<th>Grade Level</th>
						<th>Homeroom Teacher</th>
					</tr>
					<tr>
						<td>$newfirstname  $newlastname</td>
						<td>$newgradelevel</td>
						<td>$tname</td>
					</tr>";
	
				echo "</table><br>";
				echo "<form action='editstudents.php' style='display:inline;'><button type='submit'>Return to Student List</button></form>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
