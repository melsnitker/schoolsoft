<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Course Grades</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_POST["classperiod"])){
				$class_period = $_POST["classperiod"];
			    $_SESSION["classperiod"] = $class_period;
			} elseif(isset($_SESSION["classperiod"])){
				$class_period = $_SESSION["classperiod"];
			    $_SESSION["classperiod"] = $class_period;
			}
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			//Check if we arrived at this page as a teacher or admin
			if ($roleID != 8) //admin
			{
				if(isset($_SESSION["teacherbyadmin"])){
				    $teacherID = $_SESSION["teacherbyadmin"];
				    $_SESSION["teacherbyadmin"] = $teacherID;
				}
				
				//query database for teacher name
				$sql="SELECT first_name, last_name FROM teacher_info WHERE teacherID=$teacherID";
				$result=mysqli_query($con, $sql);
				$row = $result->fetch_assoc();
				$tname = $row['first_name'] . ' ' . $row['last_name'];
				
				echo "<h2>Roster - Period " . $class_period .  " - " . $tname . "</h2>";
				
				//query database for corresponding class roster
				$sql="SELECT studentID, course_grade FROM takes WHERE class_period=$class_period and teacherID=$teacherID";
				$result=mysqli_query($con, $sql) or die($con->error);
			}
			else //teacher
			{
				if(isset($_SESSION["teacherID"])){
				    $teacherID = $_SESSION["teacherID"];
				    $_SESSION["teacherID"] = $teacherID;
				}
				
				//query for subject
				$sql = "SELECT subject FROM teaches WHERE teacherID=$teacherID AND class_period=$class_period";
				$result=mysqli_query($con, $sql) or die($con->error);
				$row = $result->fetch_assoc();
				$thissubject = $row["subject"];
				
				echo "<h2>Roster - Period " . $class_period .  " - $thissubject</h2>";
				
				//query database for corresponding class roster
				$sql="SELECT studentID, course_grade FROM takes WHERE class_period=$class_period and teacherID=$teacherID";
				$result=mysqli_query($con, $sql) or die($con->error);
				
			}
			
			echo "<div>";
			//Header of table
			echo "<table>
					<tr>
						<th>Student ID No.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Course Grade</th>
					</tr>";
			
			while ($row = $result->fetch_assoc()){
				$sID = $row['studentID'];
				$query = "SELECT first_name, last_name FROM student WHERE studentID=" . $sID;
				$rslt=mysqli_query($con, $query) or die($con->error);
				$rw = $rslt->fetch_assoc();
				echo "</tr>";
				echo "<td>" . $sID . "</td>";
				echo "<td>" . $rw['first_name'] . "</td>";
				echo "<td>" . $rw['last_name'] . "</td>";
				echo "<td>" . $row['course_grade'] . "</td>";
				echo "</tr>";
			}

			echo "</table>";
			echo "</div>";
			
			echo "</div>";
			echo "<br>";
			
			$_SESSION["firstName"] = $fname;
			$_SESSION["lastName"] = $lname;
			$_SESSION["role"] = $role;
			if($roleID == 8){
				echo "<form action='gradebook.php' method='post'>";
					echo "<button type='submit' name='classperiod' value='$class_period'>Gradebook</button><br><br>";
				echo "</form>";
				$_SESSION["teacherID"] = $teacherID;
			}
			
			if($roleID == 8){
			    echo "<form action='roster.php' style='display:inline;'><button type='submit'>Back to Rosters</button></form>";
		    }else{
				echo "<form action='rosters.php' style='display:inline;'><button type='submit'>Back to Rosters</button></form>";
			}
		?>
	

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
