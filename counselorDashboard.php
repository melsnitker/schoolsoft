﻿<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Dashboard</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>


<main>
    
    <img src = "images/school2.png" alt = "" height = "192" width = "192" class = "floatright">
    <br>

	<div>
	 <?php
		$name = $_SESSION["firstName"]. ' ' . $_SESSION["lastName"];
		$role = $_SESSION["role"];
	
		echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
	?>
	
	<form action="studentcontact.php" style="display:inline;"><button type="submit">Student Contact</button></form>&emsp;
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
