<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Student Contacts</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Choose a Student to View Contact Information</h3>";
			
			//query database for students
			$sql="SELECT studentID, first_name, last_name FROM student ORDER BY last_name";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>";
				echo "<form action='contacts.php' method='post'>";
				//Header of table
				echo "<table>
					<tr>
						<th>Student Name</th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$name = $row['first_name'] . ' ' . $row['last_name'];
						//$_SESSION["studentID"] = $row['studentID'];
						$studentID = $row['studentID'];
						echo "<tr>";
						echo "<td><button type='submit' name='studentID' id='studentID' value=$studentID>$name</button></td></tr>"; 
					  }
				echo "</table>";
				echo "</form>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
