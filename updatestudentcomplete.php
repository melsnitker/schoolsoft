<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - New Employee</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Manage Students - Change Complete</h3>";
			
			if(isset($_POST["delete"])){
				
				//delete student and related records
				$thisstudent = $_POST["delete"];
				$studentname = $_SESSION["editname"];
			    
			    //delete student parents
			    $sql="SELECT parentID FROM parent_to_student WHERE studentID=" . $thisstudent;
			    $result=mysqli_query($con, $sql) or die($con->error);
			    while($row=$result->fetch_assoc()){
					
					$thisparent=$row["parentID"];
					
					$qry="DELETE FROM parent_to_student WHERE studentID=$thisstudent";
					$rslt=mysqli_query($con, $qry) or die($con->error);
					
					
					//see if parent is associated with other students
					$qry="SELECT * FROM parent_to_student WHERE parentID=$thisparent";
					$rslt=mysqli_query($con, $qry) or die($con->error);
					if(mysqli_num_rows($rslt)==0){
						
						//if not, remove parent from system completely
						$sql2="DELETE FROM parent WHERE parentID=$thisparent";
						$rslt2=mysqli_query($con, $sql2) or die($con->error);
						
					}
					
				}
			    
			    
			    //delete student emergency contacts
			    $sql="SELECT contactID FROM student_emergency WHERE studentID=$thisstudent";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    while($row=$result->fetch_assoc()){
					
					$thiscontact = $row["contactID"];
					
					$qry="DELETE FROM student_emergency WHERE studentID=$thisstudent";
					$rslt=mysqli_query($con, $qry) or die($con->error);
					
					//see if contact is associated with other students
			        $qry="SELECT * FROM student_emergency WHERE contactID=$thiscontact";
					$rslt=mysqli_query($con, $qry) or die($con->error);
					if(mysqli_num_rows($rslt)==0){
						
						//if not, remove contact from system completely
						$sql2="DELETE FROM emergency_contact WHERE contactID=$thiscontact";
						$rslt2=mysqli_query($con, $sql2) or die($con->error);
					}
					
				}
			    
			    //delete student pcps
			    $sql="DELETE FROM primary_care_contact WHERE studentID=$thisstudent";
				$result=mysqli_query($con, $sql) or die($con->error);
				
				//delete student
				$sql="DELETE FROM student WHERE studentID=$thisstudent";
			    $result=mysqli_query($con, $sql) or die($con->error);
			    
			    
			    echo "<h3>$studentname has been unregistered as a student.</h3>";
				
				}else{
			
			        $newfirstname=$_POST["first_name"];
			        $newmiddle=$_POST["middleinitial"];
			        $newlastname=$_POST["last_name"];
			        $newgradelevel=$_POST["gradelevel"];
			        $newhomeroom=$_POST["homeroomteacher"];
			        $thisstudentid = $_SESSION["editstudent"];
			
			        //update student record
			        $sql="UPDATE student SET first_name = '$newfirstname', last_name = '$newlastname', middle_initial = '$newmiddle', grade_level = $newgradelevel
			            WHERE studentID=$thisstudentid";
			        $result=mysqli_query($con, $sql) or die($con->error);
			        
			        //update takes table
			        $sql="UPDATE takes SET teacherID = $newhomeroom WHERE studentID=$thisstudentid AND subject='Homeroom'";
			        $result=mysqli_query($con, $sql) or die($con->error);
			
			        //query database for teacher name
			        $sql="SELECT first_name, last_name FROM teacher_info WHERE teacherID=$newhomeroom";
			        $result=mysqli_query($con, $sql) or die($con->error);
			        $row = $result->fetch_assoc();
		            $tname = $row['first_name'] . ' ' . $row['last_name'];
			
			        echo "<div>";
				        //Header of table
				        echo "<table>
					        <tr>
						        <th>Name</th>
						        <th>Grade Level</th>
						        <th>Homeroom Teacher</th>
					        </tr>
					        <tr>
						        <td>$newfirstname  $newlastname</td>
						        <td>$newgradelevel</td>
						        <td>$tname</td>
					        </tr>";
	
				        echo "</table><br>";
				    }
				echo "<form action='editstudents.php' style='display:inline;'><button type='submit'>Return to Student List</button></form>";
			    echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
