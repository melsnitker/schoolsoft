<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Staff Contact Information</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			
			//query database for faculty information
			$sql="SELECT roleID, first_name, last_name, phone FROM admin WHERE roleID!=8 ORDER BY roleID";
			$result=mysqli_query($con, $sql) or die($con->error);
			
			echo "<div>";
				//Header of table
				echo "<table>
					<tr>
						<th>Staff Member Name</th>
						<th>Position</th>
						<th>Phone Number</th>
					</tr>";
					while ($row = $result->fetch_assoc()){
						$name = $row['first_name'] . ' ' . $row['last_name'];
						$phone = $row['phone'];
						$thisrole = $row['roleID'];
						$qry="SELECT role FROM roles WHERE roleID=$thisrole";
						$rslt=mysqli_query($con, $qry) or die($con->error);
						$rw = $rslt->fetch_assoc();
						$thisrole=$rw['role'];
						echo "<tr><td>$name</td><td>$thisrole</td><td>$phone</td></tr>"; 
					  }
				echo "</table>";
			echo "</div>";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>