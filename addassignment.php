<!DOCTYPE html>
<html lang="en">

<head>
<title>SchoolSoft - Add Assignment</title>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<link href = "css/schoolsoft.css" rel = "stylesheet">
<!--[if lt IE9]>
<script src = "http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>

<body>
<div id = "wrapper">
<header>
    <h1>SchoolSoft</h1>
</header>

<?php
include "php/nav.php";
?>

<main>
    
    <img src = "images/school2.png" alt = "" height = "120" width = "120" class = "floatright">
    <br>

	<div>
		<?php
		   
			$fname = $_SESSION["firstName"];
			$lname = $_SESSION["lastName"];
			$name = $fname. ' ' . $lname;
			$role = $_SESSION["role"];
			$roleID = $_SESSION["RoleID"];
			
			if(isset($_SESSION["classperiod"])){
			    $class_period = $_SESSION["classperiod"];
		        $_SESSION["classperiod"] = $class_period;
			}
			if(isset($_SESSION["teacherID"])){
		        $teacherID = $_SESSION["teacherID"];
			    $_SESSION["teacherID"] = $teacherID;
			}
			
			//query for subject
		    $sql = "SELECT subject FROM teaches WHERE teacherID=$teacherID AND class_period=$class_period";
			$result=mysqli_query($con, $sql) or die($con->error);
			$row = $result->fetch_assoc();
			$thissubject = $row["subject"];
	
			echo "<h2>Wayside School - " . $name . " - " . $role . "</h2>";
			echo "<h3>Add an Assignment - Period $class_period " . " - $thissubject</h3>";
			
		    //query for current assignments
		    $sql = "SELECT DISTINCT assignment_name FROM assignment WHERE teacherID=$teacherID AND class_period=$class_period";
		    $result=mysqli_query($con, $sql) or die($con->error);
		    echo "<table><tr><th>Assignments</th></tr>";
		    while($row = $result->fetch_assoc()){
				$thisassignment = $row["assignment_name"];
				echo "<tr><td>$thisassignment</td></tr>";
			}
			echo "</table><br><br>";
			
			//don't bother with new/existing for this interface for now
			echo "<form action='insertassignment.php' method='post' style='display:inline;'>
					<label for='assignment' style='width:12em;'>Assignment to Add</label>
					<input type='text' id='assignment' name='assignment' required='required'><br><br>
					&emsp;&emsp;<button type='submit'>Save</button></form>&emsp;&emsp;
					<form action='teacherDashboard.php' style='display:inline;'><button type='submit'>Back to Home</button></form>
					
					";
			
		?>
		
	</div>
	<br>
    

</main>

<footer>
    <br>
    Copyright &copy; 2017 Melody Snitker<br>
    <a href = "mailto:pixeldarling@gmail.com">pixeldarling@gmail.com</a><br>
	<div>
		<script>
			document.write("<p>This page was last modified on: " + document.lastModified + ".</p>");
		</script>
	</div>
</footer>
</div>
</body>
</html>
